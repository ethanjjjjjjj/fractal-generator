



#include <complex>
#include <iostream>
#include <SDL2/SDL.h>
#include <png.h>

using namespace std;

//roots = z - (1+i) , z - (1 - i) , z - (-1) = z - 1 - i , z - 1 + i , z + 1 

// roots = 1 + i , 1 - i , -1

// in cpp = (1, 1) , (1,-1) , (-1 , 0)


complex<double> f(complex<double> z){
    return pow(z,3) - pow(z,2)  + complex<double> (2,0);

}

complex<double> fd(complex<double> z){
    return (complex<double>(3,0) * pow(z,2)) - (complex<double>(2,0)*z);
}


complex<double> roots[3]={complex<double>(1,1),complex<double>(1,-1),complex<double>(-1,0)};






int main(){
    int xpixels=3840;
    int ypixels=2160;
    SDL_Window* window=nullptr;
    SDL_Renderer* renderer=nullptr;

    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(xpixels,ypixels,0,&window,&renderer);
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_RenderClear(renderer);

    //complex<double> initial = (0,5);W

    
    float complexplaney=6000/3840;
    float complexplanex=6000/2160;

    //#pragma omp parallel for simd collapse(2)
    for(int y=0-(ypixels/2);y<(ypixels/2);y++){
        for(int x=0-(xpixels/2);x<(xpixels/2);x++){
            //cout<<endl;
            complex<double> initial = complex<double>((double)complexplanex*x/(xpixels),(double)complexplaney*y/(ypixels));

            for(int i=0;i<100;i++){
                initial= initial - (f(initial)/fd(initial));
        }
        //cout<<"calcedroot  "<<initial<<endl; 
        double min=abs(initial-roots[0]);
        int mindex=0;
    for(int i=0;i<3;i++){
        
        auto diff = initial-roots[i];
        double absdiff=abs(diff);
        //cout<<i<<" "<<diff<<" "<<absdiff<<endl;
        if(absdiff<min){
            min=absdiff;
            mindex=i;
            }
        

       
    }
    //cout<<"mindex: "<<mindex<<endl;
     if(mindex==0){
            SDL_SetRenderDrawColor(renderer,255,0,0,255);
            SDL_RenderDrawPoint(renderer,x+xpixels/2,y+ypixels/2);


        }
    else if(mindex==1){
            SDL_SetRenderDrawColor(renderer,0,255,0,255);
            SDL_RenderDrawPoint(renderer,x+xpixels/2,y+ypixels/2);

        }
    else if(mindex==2){
            SDL_SetRenderDrawColor(renderer,0,0,255,255);
            SDL_RenderDrawPoint(renderer,x+xpixels/2,y+ypixels/2);

        }

    }

}
while(true){
SDL_RenderPresent(renderer);
SDL_Delay(1000/60);}
}